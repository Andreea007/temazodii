using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ZodiacService
{
    public class ZodiacServer : Zodiac.ZodiacBase
    {
        private List<Tuple<string, string, string>> _signs;
        private readonly ILogger<ZodiacServer> _logger;

        public ZodiacServer(ILogger<ZodiacServer> logger)
        {
            _logger = logger;
            _signs = InitializationOfSigns();
        }

        private List<Tuple<string, string, string>> InitializationOfSigns()
        {
            List<Tuple<string, string, string>> signs = new List<Tuple<string, string, string>>();

            StreamReader streamReader = new StreamReader(Directory.GetCurrentDirectory() + "\\Resources\\ZodiacSigns.txt");

            while (!streamReader.EndOfStream)
            {
                string[] line;
                line = streamReader.ReadLine().Split(' ', '\r', '\n');

                signs.Add(new Tuple<string, string, string>(line[0], line[1], line[2]));
            }

            streamReader.Dispose();

            return signs;
        }

        private bool IsGivenDateValid(string givenDate)
        {
            var pattern = "^(((0?[1-9]|1[012])/(0?[1-9]|1\\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\\d)\\d{2}|0?2/29/((19|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(givenDate);
        }

        private int TransformStringDateToInt(string date)
        {
            int dateInt;

            if (date[0] == '0')
            {
                date = date.Remove(0, 1);
            }

            dateInt = Int32.Parse(date);

            return dateInt;
        }

        private bool IsInInterval(string startDate, string endDate, string givenDate)
        {
            int startDay = TransformStringDateToInt(startDate.Substring(startDate.LastIndexOf("/") + 1));
            int startMonth = TransformStringDateToInt(startDate.Substring(0, startDate.LastIndexOf("/")));

            int endDay = TransformStringDateToInt(endDate.Substring(endDate.LastIndexOf("/") + 1));
            int endMonth = TransformStringDateToInt(endDate.Substring(0, endDate.LastIndexOf("/")));

            int dayGiven = TransformStringDateToInt(givenDate.Substring(givenDate.LastIndexOf("/") + 1));
            int monthGiven = TransformStringDateToInt(givenDate.Substring(0, givenDate.LastIndexOf("/")));

            if (monthGiven == startMonth && dayGiven >= startDay)
            {
                return true;
            }

            if (monthGiven == endMonth && dayGiven <= endDay)
            {
                return true;
            }

            return false; ;
        }

        private string GetSign(string givenDate)
        {
            if (IsGivenDateValid(givenDate))
            {
                givenDate = givenDate.Substring(0, givenDate.LastIndexOf("/"));
                foreach (Tuple<string, string, string> tuple in _signs)
                {
                    if (IsInInterval(tuple.Item2, tuple.Item3, givenDate))
                    {
                        return tuple.Item1;
                    }
                }
            }

            return "Invalid date! Could not define sign!";
        }

        public override Task<DateResponse> GetZodiacSign(DateRequest request, ServerCallContext context)
        {
            string sign = GetSign(request.Date);

            return Task.FromResult(new DateResponse() { Sign = sign });
        }
    }
}
