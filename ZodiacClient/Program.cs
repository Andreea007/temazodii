﻿using Grpc.Net.Client;
using System;
using System.Threading.Tasks;
using ZodiacService;

namespace ZodiacClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Zodiac.ZodiacClient(channel);

            Console.WriteLine("Your date: ");
            string date = Console.ReadLine();

            while (date == null)
            {
                Console.WriteLine("Null input!\n\nIntroduce date again: ");
                date = Console.ReadLine();
            }

            var response = await client.GetZodiacSignAsync(new DateRequest { Date = date});

            Console.WriteLine();

            Console.WriteLine(response);
        }
    }
}
